score = float(input("Unesite broj izmedu 0.0 i 1.0: "))

try:
    score >= 0 and score <= 1
except:
    print("Unešeni broj nije izmedu 0 i 1")
    exit()

if(score < 0.6):
    print("Ocijena je F")
elif(score < 0.7):
    print("Ocijena je D")
elif(score < 0.8):
    print("Ocijena je C")
elif(score < 0.9):
    print("Ocijena je B")
else:
    print("Ocjena je A")