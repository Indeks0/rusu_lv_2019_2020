import re
import os
file_name = input("Unesite ime datoteke: ")
file = open(file_name)
spam_list = []

for line in file:
    if line.startswith("X-DSPAM-Confidence: "):
        x= re.findall("\d+\.\d+", line)
        spam_list.append(float(x[0]))

print("\nProsjecna vrijednost je: ", sum(spam_list)/len(spam_list))

spam_list.sort()
