import re

name = input("Unesite ime datoteke: ")

try:
    file = open(name)
except:
    print("Nije moguće otvoriti file.")

regex = r'([a-zA-Z0-9+._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)'

emails = re.findall(regex, file.read())
emails = "\n".join(emails)
emails = re.sub('@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+', '', emails)

print(emails)